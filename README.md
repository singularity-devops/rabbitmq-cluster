# rabbitmq-cluster

## Execution

Build and run necessary containers using the following command:

```bash
docker-compose up -d
```

Run the **setup.sh** script file to join rabbit-2 as a cluster using following command:

```bash
./setup.sh
```

## Screenshots

### Before Joining Cluster

### rabbit-1

![rabbit1](https://user-images.githubusercontent.com/54265853/182357753-3ee6e518-f379-4525-9c57-dfe67fdb2817.png)

### rabbit-2

![rabbit2](https://user-images.githubusercontent.com/54265853/182357766-2f3e135f-cfac-4d6f-bdba-3663ca41f32f.png)

### After Joining Cluster

### rabbit-1

![rabbitmq1-after](https://user-images.githubusercontent.com/54265853/182357812-0c559c05-1ac3-47ea-9694-7d7b4630d0f2.png)

### rabbit-2

![rabbitmq-2-after](https://user-images.githubusercontent.com/54265853/182357835-db8ec954-dba6-43fe-bd9a-e2dcc03b5fbf.png)
